# Skyline (AAI Microservice)

## Overview
Skyline is an API microservice for AAI; used to retrieve the instance data according to a model definition


## Running The App
```
make dist
java -jar target/skyline.jar
```

## Running The App In Docker

```
make dist
make image
make run
```


## Testing The Endpoints
Point your browser to `http://localhost:9525/xyzzy` or use `curl` in command line.

```
curl -v  http://localhost:9525/xyzzy
```

## Makefile
A wrapper Makefile can save some keystrokes. Type `make dist image run` to build and run the app in Docker.

More to come.


