default:
	cat ./Makefile
dist:
	mvn clean package
image:
	docker build -t skyline:latest .
run:
	docker run -p 9525:9525 --name skyline skyline:latest
run-bash:
	docker run -i -t skyline:latest /bin/bash
