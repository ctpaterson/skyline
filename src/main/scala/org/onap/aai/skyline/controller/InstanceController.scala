package org.onap.aai.skyline.controller

import java.util.{Properties}
import org.springframework.web.bind.annotation.RequestMethod.{GET, POST}
import org.springframework.web.bind.annotation.{RestController, RequestMapping, GetMapping, PostMapping, PathVariable, RequestParam}
import org.springframework.http.HttpHeaders
import collection.JavaConversions._
import java.io._
import java.util.stream.Collectors
import org.apache.http.{HttpEntity, HttpResponse}
import org.apache.http.client._
import org.apache.http.impl.client.{HttpClientBuilder, HttpClients, BasicCredentialsProvider}
import org.apache.http.client.config.{RequestConfig}
import org.apache.http.client.methods.{HttpGet}
import org.apache.http.ssl.SSLContextBuilder
import org.apache.http.conn.ssl.{SSLConnectionSocketFactory, NoopHostnameVerifier, TrustSelfSignedStrategy}
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import scala.collection.mutable.StringBuilder
import org.onap.aai.skyline.InstanceService

@RestController
class InstanceController {
  val properties = new Properties()
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"))

  val appid = properties.getProperty("skyline.name")
  
  @GetMapping(path = Array("/instances/{id}"))
  def getInstance(@PathVariable id: String,
                  @RequestParam(value = "model-id", required=false) modelId: String) = {
    new InstanceService().getInstance(id)
  }

  @GetMapping(path = Array("/instances/specs/{id}"))
  def getInstanceSpec(@PathVariable id: String,
                      @RequestParam(value = "model-id", required=false) modelId: String) = {
    new InstanceService().getInstanceSpec(id)
  }

  //  @PostMapping(path = Array("/instances/{id}"))
//  def getInstanceBySpec(@PathVariable id: String) = {
//    new InstanceService().getInstanceBySpec(id, "")
//  }
}