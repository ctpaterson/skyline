package org.onap.aai.skyline.controller

import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.{RestController, RequestMapping}
import collection.JavaConversions._

@RestController
class XyzzyController {
  @RequestMapping(path = Array("/xyzzy"))
  def xyzzy(): java.util.Map[String, String] = {
    Map("message" -> "Nothing happens.")
  }

}