package org.onap.aai.skyline.controller

import java.util.{Properties}
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.{RestController, RequestMapping, GetMapping, PathVariable, RequestParam}
import org.springframework.http.HttpHeaders
import collection.JavaConversions._
import java.io._
import java.util.stream.Collectors
import org.apache.http.{HttpEntity, HttpResponse}
import org.apache.http.client._
import org.apache.http.impl.client.{HttpClientBuilder, HttpClients, BasicCredentialsProvider}
import org.apache.http.client.config.{RequestConfig}
import org.apache.http.client.methods.{HttpGet}
import org.apache.http.ssl.SSLContextBuilder
import org.apache.http.conn.ssl.{SSLConnectionSocketFactory, NoopHostnameVerifier, TrustSelfSignedStrategy}
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import scala.collection.mutable.StringBuilder
import org.onap.aai.skyline.ModelService
import org.onap.aai.skyline.ModelSpecBuilder

@RestController
class ModelController {
  val properties = new Properties()
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"))

  val appid = properties.getProperty("skyline.name")
  
  @GetMapping(path = Array("/models"))
  def getModels(@RequestParam(value = "level", required=false) hops: String) = {
    new ModelService().getModels(hops)
  }

  @GetMapping(path = Array("/models/{id}"))
  def getModel(@PathVariable id: String,
               @RequestParam(value = "level", required=false) hops: String) = {
    new ModelService().getModel(id, hops)
  }
  
  @GetMapping(path = Array("/specs/{id}"))
  def getModelSpec(@PathVariable id: String,
                   @RequestParam(value = "level", required=false) hops: String) = {
    new ModelSpecBuilder().getModelSpecJson(id)
  }
}