package org.onap.aai.skyline

import java.util.{Properties}
import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.JsonDSL._
import org.json4s.jackson.Serialization
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.{RestController, RequestMapping, GetMapping, PathVariable, RequestParam}
import org.springframework.http.HttpHeaders
import collection.JavaConversions._
import java.io._
import java.util.stream.Collectors
import org.apache.http.{HttpEntity, HttpResponse}
import org.apache.http.client._
import org.apache.http.impl.client.{HttpClientBuilder, HttpClients, BasicCredentialsProvider}
import org.apache.http.client.config.{RequestConfig}
import org.apache.http.client.methods.{HttpGet}
import org.apache.http.ssl.SSLContextBuilder
import org.apache.http.conn.ssl.{SSLConnectionSocketFactory, NoopHostnameVerifier, TrustSelfSignedStrategy}
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import scala.collection.mutable.StringBuilder
import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.JsonDSL._
import org.json4s.jackson.Serialization

class InstanceService() extends DataService {
  val properties = new Properties()
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"))

  private val appid = properties.getProperty("skyline.name")
  private val address = properties.getProperty("skyline.instance_service.address", "httpbin.org")
  private val port = properties.getProperty("skyline.instance_service.port", "8080")
  private val uri = properties.getProperty("skyline.instance_service.uri")
  private val uid = properties.getProperty("skyline.instance_service.username")
  private val pwid = properties.getProperty("skyline.instance_service.password")
  private val connectionTimeout = properties.getProperty("skyline.instance_service.connection-timeout").toIntOrElse(5000)
  private val socketTimeout = properties.getProperty("skyline.instance_service.socket-timeout").toIntOrElse(5000)
  private val modelField = properties.getProperty("skyline.instance_service.model-field")
  private val instanceTypes = properties.getProperty("skyline.model-spec.instance-types").split(",").toList

  private val instanceServiceUri = "https://" + address + ":" + port + "/" +  uri

  implicit class JValueExtended(value: JValue) {
    def has(f: String): Boolean = (value \ f) != JNothing
    def string: String = if (JNothing != value) (compact(render(value))).replaceAll("\"", "") else ""
  }

  def getInstance(id: String): String = {
    val rootNode = prepareNode(getInstanceNode("", id))
    val modelId = rootNode.getOrElse("properties", Map[String,String]()).asInstanceOf[Map[String,String]].getOrElse("model-name-version-id", "").asInstanceOf[String]

    val modelSpec = getInstanceSpecMap(modelId)      

    time {
      println("Populating instance data based on model spec...")
      val instance = pruneUnusedEdges(populateInstance(modelSpec(0), rootNode))

      implicit val formats = org.json4s.DefaultFormats
      Serialization.write(instance)
    }
  }

  def populateInstance(modelMap: Map[String,Any],
                       rootNode: Map[String,Any]): List[Map[String,Any]] = {
    // assume root model node type matches root node type?
    val rootModelNodeId = modelMap.getOrElse("model-id","no model-id")
    val rootModelNodeName = modelMap.getOrElse("model-name", "no model-name")
    
    val rootInstNodeId = rootNode.getOrElse("id", "no inst-id")
    val rootInstNodeType = rootNode.getOrElse("type", "no inst-type")
    
    val instLeaves = rootNode.getOrElse("in", List[Map[String,Any]]()).asInstanceOf[List[Map[String,Any]]] ++ rootNode.getOrElse("out", List[Map[String,Any]]()).asInstanceOf[List[Map[String,Any]]]
    val modelLeaves = modelMap.getOrElse("model-leaves", List[Map[String,Any]]()).asInstanceOf[List[Map[String,Any]]]    
    
    if ( 1 > modelLeaves.size ) {
      List[Map[String,Any]](rootNode)
      
    } else if ( modelLeaves.size > instLeaves.size ) {
      // there are more model leaves than instance leaves - we know we don't have a fit
      List[Map[String,Any]]()

    } else {
      // sort model leaves so we check the biggest ones first to avoid subset matches
      val sortedModelLeaves = modelLeaves.sortWith(_.getOrElse("model-leaves", List[Map[String,Any]]()).asInstanceOf[List[Map[String,Any]]].size >
                                                   _.getOrElse("model-leaves", List[Map[String,Any]]()).asInstanceOf[List[Map[String,Any]]].size)

      rootNode :: fillInModelLeaves(sortedModelLeaves, instLeaves, List[Map[String,Any]]())
    }
  }
  
  def fillInModelLeaves(modelLeaves: List[Map[String,Any]],
                        instLeaves: List[Map[String,Any]],
                        accumulator: List[Map[String,Any]]): List[Map[String,Any]] = {
    if ( 1 > modelLeaves.size ) {
      accumulator

    } else if ( modelLeaves.size > instLeaves.size ) {
      List[Map[String,Any]]()

    } else {
      val ml = modelLeaves.head
      val bestcandidate = getBestCandidate(getCandidateBranches(ml, instLeaves)) match {
        case None => Map[String,Any]()
        case Some(c) => c
      }

      bestcandidate.get("node-id") match {
        case None => List[Map[String,Any]]()
        case Some(i) => {
          val candidateTree = populateInstance(ml, prepareNode(getInstanceNode("", i.asInstanceOf[String])))

          // surely this could be better
          val bestCandidateIndex = instLeaves.indexOf(bestcandidate)
          val remainingInstLeaves = instLeaves.take(bestCandidateIndex) ++ instLeaves.takeRight(instLeaves.size - bestCandidateIndex - 1)

          fillInModelLeaves(modelLeaves.tail, remainingInstLeaves, candidateTree ++ accumulator)
        }
      }          
    }
  }
  
  
  def getCandidateBranches(modelMap: Map[String,Any],
                           instLeaves: List[Map[String,Any]]): List[Map[String,Any]] = {
    val rootModelNodeId = modelMap.getOrElse("model-id","no model-id").asInstanceOf[String]
    val rootModelNodeName = modelMap.getOrElse("model-name", "no model-name").asInstanceOf[String]
    
    instLeaves.filter(il => { il.get("node-type") match {
                                case None => false
                                case Some(t) => rootModelNodeName == t.asInstanceOf[String] }})
  }
  
  def getBestCandidate(candidateList: List[Map[String,Any]]): Option[Map[String,Any]] = {
    // naive solution right now - take the first candidate
    if ( 0 < candidateList.size ) Option(candidateList(0)) else None
  }
  
  def getInstanceSpec(id: String): String = {
    implicit val formats = org.json4s.DefaultFormats
    Serialization.write(getInstanceSpecMap(id))
  }

  def getInstanceSpecMap(id: String): List[Map[String,Any]] = {
    time {
      println("Getting model spec and building map...")
      filterModelSpecMap(new ModelSpecBuilder().buildModelMap(id), instanceTypes)
    }
  }

  def getInstanceBySpec(id: String, spec: String): String = {
    ""
  }

  // assuming a flat list of vertices with "in" and "out" edges, remove any
  // edges pointing to vertices outside the list
  // nodes expected to look like (node-id and node-type added by skyline)
  // {
  //   "id": "000",
  //   "type": "scarecrow",
  //   "properties": {...},
  //   "in": [
  //           {
  //             "source": "/path/to/source",
  //             "node-id": "001",
  //             "node-type": "tin-man"
  //           }
  //         ],
  //   "out": [
  //            {
  //              "target": "/path/to/source",
  //              "node-id": "002",
  //              "node-type": "cowardly-lion"
  //            }
  //          ]
  // }
  def pruneUnusedEdges(nodes: List[Map[String,Any]]): List[Map[String,Any]] = {
    val idList = nodes.par.map(n => n.getOrElse("id", "-1")).toList
    nodes.distinct.par.map(n => {
      val inEdges = n.getOrElse("in", List[Map[String,String]]()).asInstanceOf[List[Map[String,String]]]
      val outEdges = n.getOrElse("out", List[Map[String,String]]()).asInstanceOf[List[Map[String,String]]]
      n +
        ("in" -> inEdges.par.filter(e => { idList.contains(e.getOrElse("node-id", "-2").asInstanceOf[String]) }).toList) +
        ("out" -> outEdges.par.filter(e => { idList.contains(e.getOrElse("node-id", "-2").asInstanceOf[String]) }).toList)
    }).toList
  }

  // recursively walk through list of Maps, find the index of the first element who's node.type == entityType; None if no match
  private def findTypeInList(i: Int,
                             entityType: String,
                             nodes: List[Map[String,Any]]): Option[Int] = {
    if ( nodes.isEmpty ) {
      return None
    } else {
      if (entityType == nodes.head.getOrElse("type", "")) {
        return Option(i)
      } else {
        findTypeInList(i + 1, entityType, nodes.tail)
      }
    }
  }

  // Find all members of nodes where node.type == entityType; return a List of Tuples(indexInList,node)
  private def findAllTypesInList(i: Int,
                                 entityType: String,
                                 nodes: List[Map[String,Any]],
                                 found: List[(Int,Map[String,Any])]): Option[List[(Int,Map[String,Any])]] = {
    if ( nodes.length <= i ) {
      return Option(found)
    } else {
      if (entityType == nodes(i).getOrElse("type", "")) {
        return findAllTypesInList(i + 1, entityType, nodes, found :+ (i,nodes(i)))
      } else {
        return findAllTypesInList(i + 1, entityType, nodes, found)
      }
    }
  }
  
  private def filterModelSpecMap(modelSpec: Map[String,Any],
                                 filterTypes: List[String]): List[Map[String,Any]] = {
    
    val modelType = modelSpec.get("model-type") match {
      case None => new String()
      case Some(s) => s.asInstanceOf[String]
    }

    val origModelMap = modelSpec - "model-leaves"
    val origModelLeaves = (modelSpec.get("model-leaves") match {
                            case None => List[Map[String,Any]]()
                            case Some(l) => l}).asInstanceOf[List[Map[String,Any]]]

    val filteredLeaves = origModelLeaves.par.map(m => filterModelSpecMap(m, filterTypes)).flatten.toList

    val modelLeaves = if (filteredLeaves.isEmpty) Map[String,Any]() else Map[String,Any]("model-leaves" -> filteredLeaves)

    if (filterTypes.contains(modelType)) List[Map[String,Any]]((origModelMap ++ modelLeaves)) else filteredLeaves
  }
  
  private def prepareNode(node: Map[String,Any]): Map[String,Any] = {
    val inEdges = node.getOrElse("in", List[Map[String,String]]()).asInstanceOf[List[Map[String,String]]]
    val outEdges = node.getOrElse("out", List[Map[String,String]]()).asInstanceOf[List[Map[String,String]]]

    ((node - "in") - "out") ++
      Map[String,Any]("in" -> inEdges.map(e => e ++ Map[String,Any]("node-id" -> getIdAndType(e.getOrElse("source", ""))._1,
                                                                    "node-type" -> getIdAndType(e.getOrElse("source", ""))._2)).toList) ++
      Map[String,Any]("out" -> outEdges.map(e => e ++ Map[String,Any]("node-id" -> getIdAndType(e.getOrElse("target", ""))._1,
                                                                     "node-type" -> getIdAndType(e.getOrElse("target", ""))._2)).toList)               
  } 
  
  private def getIdAndType(url: String): (String,String) = {
    // hacking the URL is very not good, but will get us through the night
    val urlElements = url.split("/")
    
    try {
      (urlElements(urlElements.length - 1), urlElements(urlElements.length - 2))
    } catch {
      case e: Exception => ("", "")
    }
  }
  
  private def getInstanceNode(nodeType: String, nodeId: String): Map[String,Any] = {
    nodeId match {
      case "00000" => Map[String,Any]("id" -> "00000",
                                      "type" -> "customer",
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/service-subscription/10000")))
      case "10000" => Map[String,Any]("id" -> "10000",
                                      "type" -> "service-subscription",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/customer/00000")),
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/service-instance/20000"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/service-instance/30000")))
      case "20000" => Map[String,Any]("id" -> "20000",
                                      "type" -> "service-instance",
                                      "properties" -> Map[String,String]("model-invariant-id" -> "034a27c7-d443-3c5b-b96c-e7a4c737940a",
                                                                         "model-name-version-id" -> "41091554-1bd4-4bc0-86ee-e8f86b82bcf7"),
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/service-subscription/10000")),
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/generic-vnf/21000")))
      case "30000" => Map[String,Any]("id" -> "30000",
                                      "type" -> "service-instance")
      case "21000" => Map[String,Any]("id" -> "21000",
                                      "type" -> "generic-vnf",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/service-instance/20000")),
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/vf-module/21100"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/vf-module/21200"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/vf-module/21300"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/vf-module/21400"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/vf-module/21500")))

      case "21100" => Map[String,Any]("id" -> "21100",
                                      "type" -> "vf-module",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/generic-vnf/21000")),
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/vserver/21110")))
      case "21110" => Map[String,Any]("id" -> "21110",
                                      "type" -> "vserver",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vf-module/21100")),
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/l-interface/21111"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/vnfc/21112"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/tenant/21113"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/image/21114"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/flavor/21115"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/pserver/21116")))
      case "21111" => Map[String,Any]("id" -> "21111",
                                      "type" -> "l-interface",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21110")))
      case "21112" => Map[String,Any]("id" -> "21112",
                                      "type" -> "vnfc",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21110")))
      case "21113" => Map[String,Any]("id" -> "21113",
                                      "type" -> "tenant",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21110")))
      case "21114" => Map[String,Any]("id" -> "21114",
                                      "type" -> "image",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21110"),
                                                                       Map[String,String]("source" -> "services/inventory/v11/vserver/21210"),
                                                                       Map[String,String]("source" -> "services/inventory/v11/vserver/21310"),
                                                                       Map[String,String]("source" -> "services/inventory/v11/vserver/21410"),
                                                                       Map[String,String]("source" -> "services/inventory/v11/vserver/21510")))
      case "21115" => Map[String,Any]("id" -> "21115",
                                      "type" -> "flavor",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21110")))
      case "21116" => Map[String,Any]("id" -> "21116",
                                      "type" -> "pserver",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21110")))

                                      
      case "21200" => Map[String,Any]("id" -> "21200",
                                      "type" -> "vf-module",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/generic-vnf/21000")),
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/vserver/21210")))
      case "21210" => Map[String,Any]("id" -> "21210",
                                      "type" -> "vserver",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vf-module/21200")),
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/l-interface/21211"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/vnfc/21212"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/tenant/21213"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/image/21114"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/flavor/21215"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/pserver/21216")))
      case "21211" => Map[String,Any]("id" -> "21211",
                                      "type" -> "l-interface",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21210")))
      case "21212" => Map[String,Any]("id" -> "21212",
                                      "type" -> "vnfc",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21210")))
      case "21213" => Map[String,Any]("id" -> "21213",
                                      "type" -> "tenant",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21210")))
      case "21214" => Map[String,Any]("id" -> "21214",
                                      "type" -> "image",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21210")))
      case "21215" => Map[String,Any]("id" -> "21215",
                                      "type" -> "flavor",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21210")))
      case "21216" => Map[String,Any]("id" -> "21216",
                                      "type" -> "pserver",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21210")))
                                      
                                      
      case "21300" => Map[String,Any]("id" -> "21300",
                                      "type" -> "vf-module",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/generic-vnf/21000")),
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/vserver/21310")))
      case "21310" => Map[String,Any]("id" -> "21310",
                                      "type" -> "vserver",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vf-module/21300")),
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/l-interface/21311"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/vnfc/21312"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/tenant/21313"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/image/21114"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/flavor/21315"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/pserver/21316")))
      case "21311" => Map[String,Any]("id" -> "21311",
                                      "type" -> "l-interface",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21310")))
      case "21312" => Map[String,Any]("id" -> "21312",
                                      "type" -> "vnfc",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21310")))
      case "21313" => Map[String,Any]("id" -> "21313",
                                      "type" -> "tenant",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21310")))
      case "21314" => Map[String,Any]("id" -> "21314",
                                      "type" -> "image",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21310")))
      case "21315" => Map[String,Any]("id" -> "21315",
                                      "type" -> "flavor",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21310")))
      case "21316" => Map[String,Any]("id" -> "21316",
                                      "type" -> "pserver",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21310")))
                                      
                                      
      case "21400" => Map[String,Any]("id" -> "21400",
                                      "type" -> "vf-module",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/generic-vnf/21000")),
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/vserver/21410")))
      case "21410" => Map[String,Any]("id" -> "21410",
                                      "type" -> "vserver",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vf-module/21400")),
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/l-interface/21411"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/vnfc/21412"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/tenant/21413"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/image/21114"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/flavor/21415"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/pserver/21416")))
      case "21411" => Map[String,Any]("id" -> "21411",
                                      "type" -> "l-interface",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21410")))
      case "21412" => Map[String,Any]("id" -> "21412",
                                      "type" -> "vnfc",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21410")))
      case "21413" => Map[String,Any]("id" -> "21413",
                                      "type" -> "tenant",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21410")))
      case "21414" => Map[String,Any]("id" -> "21414",
                                      "type" -> "image",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21410")))
      case "21415" => Map[String,Any]("id" -> "21415",
                                      "type" -> "flavor",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21410")))
      case "21416" => Map[String,Any]("id" -> "21416",
                                      "type" -> "pserver",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21410")))

                                      
      case "21500" => Map[String,Any]("id" -> "21500",
                                      "type" -> "vf-module",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/generic-vnf/21000")),
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/vserver/21510"),
                                                                       (Map[String,String]("target" -> "services/inventory/v11/l3-network/21520"))))
      case "21520" => Map[String,Any]("id" -> "21520",
                                      "type" -> "l3-network")
      case "21510" => Map[String,Any]("id" -> "21510",
                                      "type" -> "vserver",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vf-module/21500")),
                                      "out" -> List[Map[String,String]](Map[String,String]("target" -> "services/inventory/v11/l-interface/21511"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/vnfc/21512"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/tenant/21513"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/image/21114"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/flavor/21515"),
                                                                        Map[String,String]("target" -> "services/inventory/v11/pserver/21516")))
      case "21511" => Map[String,Any]("id" -> "21511",
                                      "type" -> "l-interface",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21510")))
      case "21512" => Map[String,Any]("id" -> "21512",
                                      "type" -> "vnfc",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21510")))
      case "21513" => Map[String,Any]("id" -> "21513",
                                      "type" -> "tenant",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21510")))
      case "21514" => Map[String,Any]("id" -> "21514",
                                      "type" -> "image",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21510")))
      case "21515" => Map[String,Any]("id" -> "21515",
                                      "type" -> "flavor",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21510")))
      case "21516" => Map[String,Any]("id" -> "21516",
                                      "type" -> "pserver",
                                      "in" -> List[Map[String,String]](Map[String,String]("source" -> "services/inventory/v11/vserver/21510")))
      case _ => Map[String,Any]()
    }
  }
}
