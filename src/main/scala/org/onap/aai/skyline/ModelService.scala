package org.onap.aai.skyline

import java.util.{Properties}
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.{RestController, RequestMapping, GetMapping, PathVariable, RequestParam}
import org.springframework.http.HttpHeaders
import collection.JavaConversions._
import java.io._
import java.util.stream.Collectors
import org.apache.http.{HttpEntity, HttpResponse}
import org.apache.http.client._
import org.apache.http.impl.client.{HttpClientBuilder, HttpClients, BasicCredentialsProvider}
import org.apache.http.client.config.{RequestConfig}
import org.apache.http.client.methods.{HttpGet}
import org.apache.http.ssl.SSLContextBuilder
import org.apache.http.conn.ssl.{SSLConnectionSocketFactory, NoopHostnameVerifier, TrustSelfSignedStrategy}
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import scala.collection.mutable.{StringBuilder, HashMap}

class ModelService() extends DataService {
  val cacheMap = new HashMap[String,Any]()
  val properties = new Properties()
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"))

  private val appid = properties.getProperty("skyline.name")
  private val address = properties.getProperty("skyline.model_service.address", "httpbin.org")
  private val port = properties.getProperty("skyline.model_service.port", "8080")
  private val uri = properties.getProperty("skyline.model_service.uri")
  private val modelIdPrefix = properties.getProperty("skyline.model_service.model-id-prefix")
  private val levelLabel = properties.getProperty("skyline.model_service.level-label")
  private val defaultLevel: String = properties.getProperty("skyline.model_service.default-level")
  private val uid = properties.getProperty("skyline.model_service.username")
  private val pwid = properties.getProperty("skyline.model_service.password")
  private val connectionTimeout = (properties.getProperty("skyline.model_service.connection-timeout")).toIntOrElse(5000)
  private val socketTimeout = (properties.getProperty("skyline.model_service.socket-timeout")).toIntOrElse(5000)

  private val modelServiceUri = "https://" + address + ":" + port + "/" +  uri

  def getModels(level: String = defaultLevel): String = {
    // level could be set to null
    val l = if (null == level) defaultLevel else level
    get(modelServiceUri + "?" + levelLabel + "=" + l)
  }
  
  def getModel(id: String, level: String = defaultLevel): String = {
    // level could be set to null
    val l = if (null == level) defaultLevel else level
    get(modelServiceUri + "/" + modelIdPrefix + "/" + id + "?" + levelLabel + "=" + l)
  }
  
  def get(url: String): String = {
    val result = get(url,
                     socketTimeout,
                     connectionTimeout,
                     appid,
                     "143805",
                     uid,
                     pwid)
    result match {
      case None => ""
      case Some(r) => r.asInstanceOf[String]
    }
  }
}
