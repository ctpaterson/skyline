package org.onap.aai.skyline

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SkylineApp

object SkylineApp {
  def main(args: Array[String]): Unit = SpringApplication.run(classOf[SkylineApp], args: _*)
}