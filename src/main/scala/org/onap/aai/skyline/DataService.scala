package org.onap.aai.skyline

import java.util.{Properties}
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.{RestController, RequestMapping, GetMapping, PathVariable, RequestParam}
import org.springframework.http.HttpHeaders
import collection.JavaConversions._
import java.io._
import java.util.stream.Collectors
import org.apache.http.{HttpEntity, HttpResponse}
import org.apache.http.client._
import org.apache.http.impl.client.{HttpClientBuilder, HttpClients, BasicCredentialsProvider}
import org.apache.http.client.config.{RequestConfig}
import org.apache.http.client.methods.{HttpGet}
import org.apache.http.ssl.SSLContextBuilder
import org.apache.http.conn.ssl.{SSLConnectionSocketFactory, NoopHostnameVerifier, TrustSelfSignedStrategy}
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import scala.collection.mutable.StringBuilder

trait DataService {
  
  implicit class XString(s: String) {
    def toIntOrElse(i: Int): Int = try {
      s.toInt
    } catch {
      case e: Exception => i
    }
  }

  def time[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) / 1000000 + "ms")
    result
  }

  protected def get(uri: String,
                    socketTimeout: Int,
                    connectionTimeout: Int,
                    appId: String,
                    transactionId: String,
                    uid: String,
                    pwid: String,
                    attempt: Int = 1): Option[String] = {
    val sslContext = SSLContextBuilder.create()
                                      .loadTrustMaterial(new TrustSelfSignedStrategy())
                                      .build()

    val provider = new BasicCredentialsProvider()
    provider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(uid, pwid))

    val httpClient = HttpClients.custom()
                                .setSSLSocketFactory(new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier()))
                                .setDefaultCredentialsProvider(provider)
                                .build()

    val requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                                     .setSocketTimeout(socketTimeout)
                                     .setConnectTimeout(connectionTimeout)
                                     .setConnectionRequestTimeout(connectionTimeout)
                                     .build()

    val httpRequest = new HttpGet(uri)
    httpRequest.setConfig(requestConfig)
    httpRequest.setHeader("X-FromAppId", appId)
    httpRequest.setHeader("X-TransactionID", transactionId)
    httpRequest.setHeader(HttpHeaders.ACCEPT, "application/json")

    val response = httpClient.execute(httpRequest)
    val inputStream = response.getEntity.getContent()
    val responseString = io.Source.fromInputStream(inputStream).getLines.mkString

    inputStream.close()
    response.close()
    response.getStatusLine.getStatusCode match {
      case c if (200 == c) => Option(responseString)
      case c if (399 < c && 500 > c) => {
        println("HTTP Request: " + httpRequest.toString())
        println("HTTP Response: " + response.toString())
        None
      }
      case c if (499 < c && 600 > c) => {
        println("HTTP Request: " + httpRequest.toString())
        println("HTTP Response: " + response.toString())
        if (3 <= attempt) {
          println("Max number of attempts made.")
          None
        } else {
          println("Retrying...")
          get(uri, socketTimeout, connectionTimeout, appId, transactionId, uid, pwid, attempt + 1)
        }
      }
    }
  }
}
