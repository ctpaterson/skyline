package org.onap.aai.skyline

import java.util.{Properties}
import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.JsonDSL._
import org.json4s.jackson.Serialization
import java.{util => ju}
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer

class ModelSpecBuilder() {
  val properties = new Properties()
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"))

  private val use_local_leaf = properties.getProperty("skyline.model-spec.use-local-leaf")
  private val model_types = properties.getProperty("skyline.model-spec.model-types")
  private val instance_types = properties.getProperty("skyline.model-spec.instance-types")

  implicit class JValueExtended(value: JValue) {
    def has(f: String): Boolean = (value \ f) != JNothing
    def string: String = if (JNothing != value) (compact(render(value))).replaceAll("\"", "") else ""
  }
  
  implicit class StringPlus(s: String) {
    def stripQ: String = s.replaceAll("\"", "")
  }

  def getModelSpecJson(modelid: String): String = {
    implicit val formats = org.json4s.DefaultFormats
    Serialization.write(buildModelMap(modelid))
  }

  def buildModelMap(modelid: String): Map[String,Any] = {
    val json = parse(getModelSpec(modelid))
    val leaves = ListBuffer[Map[String,Any]]()

    if (json.has("relationship-list")) {
      leaves ++= followRelationships(json)
    }
    
    if (json.has("model-elements")) {
      leaves ++= followElements(json)
    }

    Map[String,Any]("model-id" -> modelid,
                    "model-type" -> (json \ "model-type").string,
                    "model-name" -> (json \ "model-name").string,
                    "model-leaves" -> leaves.toList)
  }
  
  def buildModelElementMap(json: JValue): Map[String,Any] = {
    val leaves = ListBuffer[Map[String,Any]]()

    val rMap = if (json.has("relationship-list"))
                 followRelationship(json)
               else
                 Map[String,Any]()

    if (json.has("model-elements")) {
      val leaves = followElements(json) ++ rMap.get("model-leaves").getOrElse(List[Map[String,Any]]()).asInstanceOf[List[Map[String,Any]]]
      rMap + ("model-leaves" -> leaves)
    } else {
      rMap
    }
  }

  def buildModelMapFromRelationship(json: JValue): Map[String,Any] = {
    // STRICT assumption about this struture from the model service, including the vector order in 
    // "related-to-property":
    //   "relationship": [
    //       {   
    //           "related-to": "model",
    //           "related-link": "https://host:port/aai/v00/service-design-and-creation/models/model/uuid/",
    //           "relationship-data": [
    //               {   
    //                   "relationship-key": "model.model-name-version-id",
    //                   "relationship-value": "uuid"
    //               }   
    //           ],  
    //           "related-to-property": [
    //               {   
    //                   "property-key": "model.model-name",
    //                   "property-value": "l-interface"
    //               },  
    //               {   
    //                   "property-key": "model.model-type",
    //                   "property-value": "widget"
    //               }   
    //           ]
    //       }
    //   ]
    Map[String,Any]("model-id" -> (((json \ "relationship-data")(0)) \ "relationship-value").string,
                    "model-name" -> (((json \ "related-to-property")(0)) \ "property-value").string,
                    "model-type" -> (((json \ "related-to-property")(1)) \ "property-value").string)    
  }
  
  
  private def followRelationship(json: JValue): Map[String,Any] = {
    // The method will assume only one relationship - log a warning if there's more
    scala.util.control.Exception.ignoring(classOf[IndexOutOfBoundsException]) {
      if (JNothing != ((json \ "relationship-list" \ "relationship")(1))) {
        println("WARNING - evalulating single relationship, but others are available: " + json)
      }
    }

    implicit val formats = DefaultFormats
    val relJson = ((json \ "relationship-list" \ "relationship")(0))
    if ("model".equalsIgnoreCase((relJson \ "related-to").string)) {
      if ("true" == use_local_leaf) {
        buildModelMapFromRelationship(relJson)
      } else {
        buildModelMap((((relJson \ "relationship-data")(0)) \ "relationship-value").string)
      }
    } else {
      Map[String,Any]()
    }
  }

  private def followRelationships(json: JValue): List[Map[String,Any]] = {
    implicit val formats = DefaultFormats
    
    (json \ "relationship-list" \ "relationship").asInstanceOf[JArray].arr.filter(
        r => "model".equalsIgnoreCase((r \ "related-to").string)).par.map(
            r => buildModelMap(((((r \ "relationship-data")(0)) \ "relationship-value").string))).toList
  }

  private def followElements(json: JValue): List[Map[String,Any]] = {
    implicit val formats = DefaultFormats
    (json \ "model-elements" \ "model-element").asInstanceOf[JArray].arr.par.map(
        e => buildModelElementMap(e)).toList
  }

  private def getModelSpec(modelid: String): String = {
    new ModelService().getModel(modelid)
  }
}